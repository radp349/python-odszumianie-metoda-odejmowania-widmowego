import numpy as np
import scipy.io.wavfile as wav
import matplotlib.pyplot as plt
from scipy import linalg
from scipy.fft import fft, ifft

# Dzielimy nagranie na tak szerokie segmenty
width = 20 #ms

# Ilość segmentów na początku nagrania, które zawierają wyłącznie szum (w tych nagraniach na początku jest 1s samego szumu) 
initial_noise = int(1000 / width)

# Wczytywanie
fso, org = wav.read('src/sample31.wav')
fsm, mix = wav.read('src/mixed_white.wav')
fsn, noi = wav.read('src/szum_white.wav')

# Normalizacja
original = np.array(org/np.linalg.norm(org))
mixed = np.array(mix/np.linalg.norm(mix))
noise = np.array(noi/np.linalg.norm(noi))

# Centrowanie
original -= np.mean(original)
mixed -= np.mean(mixed)
noise -= np.mean(noise)


# Obliczanie długości nagranań w sekundach - tutaj mają po 31s
lengtho = len(original)/fso
lengthm = len(mixed)/fsm
lengthn = len(noise)/fsn

to = np.linspace(0, lengtho, len(original))
tm = np.linspace(0, lengthm, len(mixed))
tn = np.linspace(0, lengthn, len(noise))
fig, axs = plt.subplots(2, 2, figsize=(12, 8))
axs[0, 0].plot(to, original)
axs[0, 0].set_title('Niezaszumiona Muzyka')
axs[0, 1].plot(tn, noise)
axs[0, 1].set_title('Szum')
axs[1, 0].plot(tm, mixed)
axs[1, 0].set_title('Zaszumiona Muzyka')
axs[1, 1].plot(tm, mixed)
axs[1, 1].plot(to, original)
axs[1, 1].set_title('Oryginał a Zaszumienie')
plt.tight_layout()
plt.show()

# Na tyle segmentów trzeba podzielić nagranie
rows = int(lengthm * 1000 / width)

# Każdy segment będzie zawierać tyle próbek
columns = int(fsm * width / 1000)


# Krok 1 - Estymacja widmowej gęstości mocy szumu
matrixm = np.reshape(mixed,(int(rows),int(columns))) 
Sz = []
for i in range(initial_noise):
    fourier = np.array(fft(matrixm[i]))
    Sz.append((1/columns)*np.square(np.absolute(fourier)))
    
Sz = np.array(Sz)
Szmean = Sz.mean(axis=0)
w = np.linspace(0, 1, int(columns))
tz = w*fsm

plt.plot(tz,10*np.log10(Szmean))
plt.title("Oszacowanie widmowej gęstości mocy szumu")
plt.xlabel("f(Hz)")
plt.ylabel("dB")
plt.show()


Sz = Sz[:,0:int((columns/2))]
Szmean = Sz.mean(axis=0)


# Krok 2 - Estymacja widmowej gęstości mocy zaszumionego nagrania 
Sx = []
Denoised = []
for i in range(initial_noise,rows):
    fouriery = np.array(fft(matrixm[i]))
    Sfouriery = ((1/columns)*np.square(np.absolute(fouriery)))
    Sfouriery = Sfouriery[0:int((columns/2))]
    
    # Krok 3 - Estymacja widmowej gęstości mocy dla nagrania bez szumu
    Sxx = Sfouriery - Szmean
    for i in range(len(Sxx)):
        if Sxx[i] < 0:
            Sxx[i] = 0
            
    Sx.append(Sxx)
            
    # Krok 4 - Projektowanie filtru odszumiającego
    A = np.sqrt(Sxx/Sfouriery)
    Amirror = np.flip(A)
    A = np.concatenate([A, Amirror])

    
    # Krok 5 - Estymacja pojedynczego segmentu odszumionego nagrania
    x = A*fouriery
    x = np.real(ifft(x))
    Denoised.append(x)

Denoised = np.array(Denoised)


# plt.plot(tz,Sfouriery)
# plt.title("Oszacowanie widmowej gęstości mocy odszumionego utworu")
# plt.xlabel("f(Hz)")
# plt.ylabel("|Sx|")
# plt.show()  

Denoised = Denoised.flatten()

# Okno Hanna - overlap-add 50%  (CHYBA SIĘ ŹLE OKNA NAKŁADAJĄ :/ BO EFEKT JEST STRASZNY)
# tt = np.array(range(columns))
# w = 0.5*(np.ones(columns) - np.cos(np.pi*tt*2/columns))
# for i in range((rows*2)-1-initial_noise*2):
#     Denoised[int(i*columns/2):int((columns)+(i*columns/2))] *= w

f = np.sort(np.abs(Denoised))[::-1]
g = np.mean(f[:10000])
for i in range(len(Denoised)):
    if(Denoised[i])>g:
        Denoised[i]=g
    elif(Denoised[i]<-g):
        Denoised[i]=-g
        
scaled = np.int16(Denoised / np.max(np.abs(Denoised)) * 32767)

wav.write('src/rezultat.wav',fsm, scaled)


