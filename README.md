# README 

Ten skrypt służy do odszumiania plików dźwiękowych. Wykorzystuje on estymację widmowej gęstości mocy oraz technikę FFT (Fast Fourier Transform) do odszumiania audio.

## Kody

Poniżej znajduje się opis poszczególnych części kodu.

Na początku importujemy potrzebne biblioteki - `numpy`, `scipy.io.wavfile` do obsługi plików .wav, `matplotlib.pyplot` do generowania wykresów, `scipy.linalg` do operacji na macierzach, `scipy.fft` do operacji na transformatę Fouriera.

Następnie określamy szerokość segmentów, na które będziemy dzielić nagranie, a także liczbę segmentów początkowych, które zawierają tylko szum. 

Po tym wczytujemy pliki .wav, które zawierają oryginalne, zaszumione i szumowe nagrania. Dokonujemy ich normalizacji, a następnie centrujemy, odejmując średnią wartość z każdej próbki.

Tworzymy wykresy tych trzech sygnałów, aby wizualnie zobaczyć różnicę między nimi.

Następnie podzielmy zaszumione nagranie na segmenty. 

### Krok 1
Estymujemy widmową gęstość mocy szumu dla pierwszych kilku segmentów (które zakładamy, że zawierają tylko szum). Używamy do tego transformaty Fouriera.

### Krok 2
Estymujemy widmową gęstość mocy zaszumionego nagrania dla każdego segmentu.

### Krok 3
Estymujemy widmową gęstość mocy nagrania bez szumu, odejmując estymowaną widmową gęstość mocy szumu od estymowanej widmową gęstości mocy zaszumionego nagrania.

### Krok 4
Projektujemy filtr odszumiający na podstawie estymowanej widmowej gęstości mocy.

### Krok 5
Estymujemy segment odszumionego nagrania, mnożąc filtr przez zaszumione nagranie, a następnie wykonujemy odwrotną transformatę Fouriera.

Na końcu spłaszczamy wynikowe nagranie do jednowymiarowej tablicy i skalujemy wartości do zakresu -32767 do 32767, aby można było je zapisać jako 16-bitowy plik .wav. Zapisujemy wynik do pliku.

## Wymagania

Aby uruchomić ten skrypt, potrzebujesz następujących bibliotek:

- numpy
- scipy
- matplotlib

## Użytkowanie

Ten skrypt jest przeznaczony do uruchomienia jako samodzielny plik Python. Musisz dostarczyć trzy pliki .wav: oryginalne nagranie, zaszumione nagranie i nagranie zawierające tylko szum. Te pliki są wczytywane z folderu `src/`. 

Po uruchomieniu skryptu, odszumione nagranie zostanie zapisane jako `src/rezultat.wav`.

## Ograniczenia

Ten skrypt zakłada, że na początku zaszumionego nagrania jest tylko szum, który jest używany do estymacji widmowej gęstości mocy szumu. Jeśli to założenie nie jest spełnione, wyniki mogą być niewłaściwe. 

Skrypt jest obecnie zaprojektowany do pracy z dźwiękiem mono. Nie obsługuje dźwięku stereo. 

Wynikowe nagranie jest skalowane do zakresu -32767 do 32767. Może to spowodować zniekształcenia, jeśli oryginalne nagranie miało wartości poza tym zakresem.
